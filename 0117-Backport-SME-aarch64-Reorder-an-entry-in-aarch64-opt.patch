From 4a2d0bdf5c9a5f4ee615c1d0768cb2e8a3dfef4a Mon Sep 17 00:00:00 2001
From: Richard Sandiford <richard.sandiford@arm.com>
Date: Thu, 29 Sep 2022 11:32:54 +0100
Subject: [PATCH 018/157] [Backport][SME] aarch64: Reorder an entry in
 aarch64-option-extensions.def

Reference: https://gcc.gnu.org/git/?p=gcc.git;a=commit;h=c067c474f85b1e9c56fb34dd51ef0eec9221b766

aarch64-option-extensions.def was topologically sorted except
for one case: crypto came before its aes and sha2 dependencies.
This patch moves crypto after sha2 instead.

gcc/
	* config/aarch64/aarch64-option-extensions.def: Move crypto
	after sha2.

gcc/testsuite/
	* gcc.target/aarch64/cpunative/native_cpu_0.c: Expect +crypto
	to come after +crc.
	* gcc.target/aarch64/cpunative/native_cpu_13.c: Likewise.
	* gcc.target/aarch64/cpunative/native_cpu_16.c: Likewise.
	* gcc.target/aarch64/cpunative/native_cpu_17.c: Likewise.
	* gcc.target/aarch64/cpunative/native_cpu_6.c: Likewise.
	* gcc.target/aarch64/cpunative/native_cpu_7.c: Likewise.
	* gcc.target/aarch64/options_set_2.c: Likewise.
	* gcc.target/aarch64/options_set_3.c: Likewise.
	* gcc.target/aarch64/options_set_4.c: Likewise.
---
 .../aarch64/aarch64-option-extensions.def     | 20 +++++++++----------
 .../aarch64/cpunative/native_cpu_0.c          |  2 +-
 .../aarch64/cpunative/native_cpu_13.c         |  2 +-
 .../aarch64/cpunative/native_cpu_16.c         |  2 +-
 .../aarch64/cpunative/native_cpu_17.c         |  2 +-
 .../aarch64/cpunative/native_cpu_6.c          |  2 +-
 .../aarch64/cpunative/native_cpu_7.c          |  2 +-
 .../gcc.target/aarch64/options_set_2.c        |  2 +-
 .../gcc.target/aarch64/options_set_3.c        |  2 +-
 .../gcc.target/aarch64/options_set_4.c        |  4 ++--
 10 files changed, 20 insertions(+), 20 deletions(-)

diff --git a/gcc/config/aarch64/aarch64-option-extensions.def b/gcc/config/aarch64/aarch64-option-extensions.def
index b98008127..df2c8d19b 100644
--- a/gcc/config/aarch64/aarch64-option-extensions.def
+++ b/gcc/config/aarch64/aarch64-option-extensions.def
@@ -76,16 +76,6 @@ AARCH64_OPT_EXTENSION("simd", AARCH64_FL_SIMD, AARCH64_FL_FP, \
 		      AARCH64_FL_I8MM | AARCH64_FL_F32MM | AARCH64_FL_F64MM, \
 		      false, "asimd")
 
-/* Enabling "crypto" also enables "fp", "simd", "aes" and "sha2".
-   Disabling "crypto" disables "crypto", "aes", "sha2", "sha3" and "sm3/sm4",
-   "sve2-aes", "sve2-sha3", "sve2-sm4".  */
-AARCH64_OPT_EXTENSION("crypto", AARCH64_FL_CRYPTO, AARCH64_FL_FP | \
-		      AARCH64_FL_SIMD | AARCH64_FL_AES | AARCH64_FL_SHA2, \
-		      AARCH64_FL_AES | AARCH64_FL_SHA2 | AARCH64_FL_SHA3 | \
-		      AARCH64_FL_SM4 | AARCH64_FL_SVE2_AES | \
-		      AARCH64_FL_SVE2_SHA3 | AARCH64_FL_SVE2_SM4, true, \
-		      "aes pmull sha1 sha2")
-
 /* Enabling or disabling "crc" only changes "crc".  */
 AARCH64_OPT_EXTENSION("crc", AARCH64_FL_CRC, 0, 0, false, "crc32")
 
@@ -127,6 +117,16 @@ AARCH64_OPT_EXTENSION("sha2", AARCH64_FL_SHA2, AARCH64_FL_FPSIMD, \
 		      AARCH64_FL_CRYPTO | AARCH64_FL_SHA3 | \
 		      AARCH64_FL_SVE2_SHA3, false, "sha1 sha2")
 
+/* Enabling "crypto" also enables "fp", "simd", "aes" and "sha2".
+   Disabling "crypto" disables "crypto", "aes", "sha2", "sha3" and "sm3/sm4",
+   "sve2-aes", "sve2-sha3", "sve2-sm4".  */
+AARCH64_OPT_EXTENSION("crypto", AARCH64_FL_CRYPTO, AARCH64_FL_FP | \
+		      AARCH64_FL_SIMD | AARCH64_FL_AES | AARCH64_FL_SHA2, \
+		      AARCH64_FL_AES | AARCH64_FL_SHA2 | AARCH64_FL_SHA3 | \
+		      AARCH64_FL_SM4 | AARCH64_FL_SVE2_AES | \
+		      AARCH64_FL_SVE2_SHA3 | AARCH64_FL_SVE2_SM4, true, \
+		      "aes pmull sha1 sha2")
+
 /* Enabling "sha3" enables "simd" and "sha2".
    Disabling "sha3" disables "sha3" and "sve2-sha3".  */
 AARCH64_OPT_EXTENSION("sha3", AARCH64_FL_SHA3, AARCH64_FL_FPSIMD | \
diff --git a/gcc/testsuite/gcc.target/aarch64/cpunative/native_cpu_0.c b/gcc/testsuite/gcc.target/aarch64/cpunative/native_cpu_0.c
index f155f51ba..8499f87c3 100644
--- a/gcc/testsuite/gcc.target/aarch64/cpunative/native_cpu_0.c
+++ b/gcc/testsuite/gcc.target/aarch64/cpunative/native_cpu_0.c
@@ -7,6 +7,6 @@ int main()
   return 0;
 }
 
-/* { dg-final { scan-assembler {\.arch armv8-a\+crypto\+crc\+dotprod} } } */
+/* { dg-final { scan-assembler {\.arch armv8-a\+crc\+dotprod\+crypto} } } */
 
 /* Test a normal looking procinfo.  */
diff --git a/gcc/testsuite/gcc.target/aarch64/cpunative/native_cpu_13.c b/gcc/testsuite/gcc.target/aarch64/cpunative/native_cpu_13.c
index b7b3a8e13..551669091 100644
--- a/gcc/testsuite/gcc.target/aarch64/cpunative/native_cpu_13.c
+++ b/gcc/testsuite/gcc.target/aarch64/cpunative/native_cpu_13.c
@@ -7,6 +7,6 @@ int main()
   return 0;
 }
 
-/* { dg-final { scan-assembler {\.arch armv8-a\+crypto\+crc\+dotprod} } } */
+/* { dg-final { scan-assembler {\.arch armv8-a\+crc\+dotprod\+crypto} } } */
 
 /* Test one with mixed order of feature bits.  */
diff --git a/gcc/testsuite/gcc.target/aarch64/cpunative/native_cpu_16.c b/gcc/testsuite/gcc.target/aarch64/cpunative/native_cpu_16.c
index a424e7c56..2f963bb23 100644
--- a/gcc/testsuite/gcc.target/aarch64/cpunative/native_cpu_16.c
+++ b/gcc/testsuite/gcc.target/aarch64/cpunative/native_cpu_16.c
@@ -7,6 +7,6 @@ int main()
   return 0;
 }
 
-/* { dg-final { scan-assembler {\.arch armv8-a\+crypto\+crc\+dotprod\+sve2} } } */
+/* { dg-final { scan-assembler {\.arch armv8-a\+crc\+dotprod\+crypto\+sve2} } } */
 
 /* Test a normal looking procinfo.  */
diff --git a/gcc/testsuite/gcc.target/aarch64/cpunative/native_cpu_17.c b/gcc/testsuite/gcc.target/aarch64/cpunative/native_cpu_17.c
index c269c5fef..c68a697aa 100644
--- a/gcc/testsuite/gcc.target/aarch64/cpunative/native_cpu_17.c
+++ b/gcc/testsuite/gcc.target/aarch64/cpunative/native_cpu_17.c
@@ -7,6 +7,6 @@ int main()
   return 0;
 }
 
-/* { dg-final { scan-assembler {\.arch armv8-a\+crypto\+crc\+dotprod\+sve2} } } */
+/* { dg-final { scan-assembler {\.arch armv8-a\+crc\+dotprod\+crypto\+sve2} } } */
 
 /* Test a normal looking procinfo.  */
diff --git a/gcc/testsuite/gcc.target/aarch64/cpunative/native_cpu_6.c b/gcc/testsuite/gcc.target/aarch64/cpunative/native_cpu_6.c
index da72052e6..7608e8845 100644
--- a/gcc/testsuite/gcc.target/aarch64/cpunative/native_cpu_6.c
+++ b/gcc/testsuite/gcc.target/aarch64/cpunative/native_cpu_6.c
@@ -7,7 +7,7 @@ int main()
   return 0;
 }
 
-/* { dg-final { scan-assembler {\.arch armv8-a\+crypto\+fp16} } } */
+/* { dg-final { scan-assembler {\.arch armv8-a\+fp16\+crypto} } } */
 
 /* Test one where the feature bits for crypto and fp16 are given in
    same order as declared in options file.  */
diff --git a/gcc/testsuite/gcc.target/aarch64/cpunative/native_cpu_7.c b/gcc/testsuite/gcc.target/aarch64/cpunative/native_cpu_7.c
index 96ad4c14d..72b14b4f6 100644
--- a/gcc/testsuite/gcc.target/aarch64/cpunative/native_cpu_7.c
+++ b/gcc/testsuite/gcc.target/aarch64/cpunative/native_cpu_7.c
@@ -7,7 +7,7 @@ int main()
   return 0;
 }
 
-/* { dg-final { scan-assembler {\.arch armv8-a\+crypto\+fp16} } } */
+/* { dg-final { scan-assembler {\.arch armv8-a\+fp16\+crypto} } } */
 
 /* Test one where the crypto and fp16 options are specified in different
    order from what is in the options file.  */
diff --git a/gcc/testsuite/gcc.target/aarch64/options_set_2.c b/gcc/testsuite/gcc.target/aarch64/options_set_2.c
index 3476febce..f82cb5f78 100644
--- a/gcc/testsuite/gcc.target/aarch64/options_set_2.c
+++ b/gcc/testsuite/gcc.target/aarch64/options_set_2.c
@@ -6,6 +6,6 @@ int main ()
   return 0;
 }
 
-/* { dg-final { scan-assembler-times {\.arch armv8\.2\-a\+crypto\+crc} 1 } } */
+/* { dg-final { scan-assembler-times {\.arch armv8\.2\-a\+crc\+crypto} 1 } } */
 
 /* Check to see if crc and crypto are maintained if crypto specified.  */
diff --git a/gcc/testsuite/gcc.target/aarch64/options_set_3.c b/gcc/testsuite/gcc.target/aarch64/options_set_3.c
index 4558339f1..7d350cfa3 100644
--- a/gcc/testsuite/gcc.target/aarch64/options_set_3.c
+++ b/gcc/testsuite/gcc.target/aarch64/options_set_3.c
@@ -6,6 +6,6 @@ int main ()
   return 0;
 }
 
-/* { dg-final { scan-assembler-times {\.arch armv8\.2\-a\+crypto\+crc} 1 } } */
+/* { dg-final { scan-assembler-times {\.arch armv8\.2\-a\+crc\+crypto} 1 } } */
 
 /* Check if smallest set is maintained when outputting. */
diff --git a/gcc/testsuite/gcc.target/aarch64/options_set_4.c b/gcc/testsuite/gcc.target/aarch64/options_set_4.c
index 15514bfe9..5370e02e1 100644
--- a/gcc/testsuite/gcc.target/aarch64/options_set_4.c
+++ b/gcc/testsuite/gcc.target/aarch64/options_set_4.c
@@ -6,7 +6,7 @@ int main ()
   return 0;
 }
 
-/* { dg-final { scan-assembler-times {\.arch armv8\.2\-a\+crypto\+crc} 1 } } */
+/* { dg-final { scan-assembler-times {\.arch armv8\.2\-a\+crc\+crypto} 1 } } */
 
 /* Check if individual bits that make up a grouping is specified that only the
-   grouping is kept. */
\ No newline at end of file
+   grouping is kept. */
-- 
2.33.0

