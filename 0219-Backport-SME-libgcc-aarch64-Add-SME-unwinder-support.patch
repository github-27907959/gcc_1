From 310c8b5aaedad1430146fed9d8992201278164a6 Mon Sep 17 00:00:00 2001
From: Szabolcs Nagy <szabolcs.nagy@arm.com>
Date: Fri, 29 Sep 2023 13:55:51 +0100
Subject: [PATCH 120/157] [Backport][SME] libgcc: aarch64: Add SME unwinder
 support

Reference: https://gcc.gnu.org/git/?p=gcc.git;a=commit;h=91d68665b8b7a5dffd0bbf8cd1f74c3c41d4c2d8

To support the ZA lazy save scheme, the PCS requires the unwinder to
reset the SME state to PSTATE.SM=0, PSTATE.ZA=0, TPIDR2_EL0=0 on entry
to an exception handler. We use the __arm_za_disable SME runtime call
unconditionally to achieve this.
https://github.com/ARM-software/abi-aa/blob/main/aapcs64/aapcs64.rst#exceptions

The hidden alias is used to avoid a PLT and avoid inconsistent VPCS
marking (we don't rely on special PCS at the call site). In case of
static linking the SME runtime init code is linked in code that raises
exceptions.

libgcc/ChangeLog:

	* config/aarch64/__arm_za_disable.S: Add hidden alias.
	* config/aarch64/aarch64-unwind.h: Reset the SME state before
	EH return via the _Unwind_Frames_Extra hook.
---
 libgcc/config/aarch64/__arm_za_disable.S |  5 +++++
 libgcc/config/aarch64/aarch64-unwind.h   | 16 ++++++++++++++++
 2 files changed, 21 insertions(+)

diff --git a/libgcc/config/aarch64/__arm_za_disable.S b/libgcc/config/aarch64/__arm_za_disable.S
index cff5b9cec..03fc28a39 100644
--- a/libgcc/config/aarch64/__arm_za_disable.S
+++ b/libgcc/config/aarch64/__arm_za_disable.S
@@ -63,3 +63,8 @@ ENTRY (__arm_za_disable)
 L(end):
 	ret
 END (__arm_za_disable)
+
+/* Hidden alias used by the unwinder.  */
+.global __libgcc_arm_za_disable
+.hidden __libgcc_arm_za_disable
+.set __libgcc_arm_za_disable, __arm_za_disable
diff --git a/libgcc/config/aarch64/aarch64-unwind.h b/libgcc/config/aarch64/aarch64-unwind.h
index 40b22d3c2..bfa695dcb 100644
--- a/libgcc/config/aarch64/aarch64-unwind.h
+++ b/libgcc/config/aarch64/aarch64-unwind.h
@@ -87,4 +87,20 @@ aarch64_frob_update_context (struct _Unwind_Context *context,
   return;
 }
 
+/* SME runtime function local to libgcc, streaming compatible
+   and preserves more registers than the base PCS requires, but
+   we don't rely on that here.  */
+__attribute__ ((visibility ("hidden")))
+void __libgcc_arm_za_disable (void);
+
+/* Disable the SME ZA state in case an unwound frame used the ZA
+   lazy saving scheme.  */
+#undef _Unwind_Frames_Extra
+#define _Unwind_Frames_Extra(x)				\
+  do							\
+    {							\
+      __libgcc_arm_za_disable ();			\
+    }							\
+  while (0)
+
 #endif /* defined AARCH64_UNWIND_H && defined __ILP32__ */
-- 
2.33.0

