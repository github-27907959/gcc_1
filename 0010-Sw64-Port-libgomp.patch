From 6ecc701c02c54cd1af013e70aef7ccf768f42da2 Mon Sep 17 00:00:00 2001
From: swcompiler <lc@wxiat.com>
Date: Mon, 25 Nov 2024 16:53:22 +0800
Subject: [PATCH 10/16] Sw64 Port: libgomp

---
 libgomp/config/linux/sw_64/futex.h | 102 +++++++++++++++++++++++++++++
 libgomp/configure                  |   6 ++
 libgomp/configure.tgt              |   4 ++
 libgomp/libgomp.spec.in            |   2 +-
 4 files changed, 113 insertions(+), 1 deletion(-)
 create mode 100644 libgomp/config/linux/sw_64/futex.h

diff --git a/libgomp/config/linux/sw_64/futex.h b/libgomp/config/linux/sw_64/futex.h
new file mode 100644
index 000000000..258f38289
--- /dev/null
+++ b/libgomp/config/linux/sw_64/futex.h
@@ -0,0 +1,102 @@
+/* Copyright (C) 2005-2022 Free Software Foundation, Inc.
+   Contributed by Richard Henderson <rth@redhat.com>.
+
+   This file is part of the GNU Offloading and Multi Processing Library
+   (libgomp).
+
+   Libgomp is free software; you can redistribute it and/or modify it
+   under the terms of the GNU General Public License as published by
+   the Free Software Foundation; either version 3, or (at your option)
+   any later version.
+
+   Libgomp is distributed in the hope that it will be useful, but WITHOUT ANY
+   WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
+   FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
+   more details.
+
+   Under Section 7 of GPL version 3, you are granted additional
+   permissions described in the GCC Runtime Library Exception, version
+   3.1, as published by the Free Software Foundation.
+
+   You should have received a copy of the GNU General Public License and
+   a copy of the GCC Runtime Library Exception along with this program;
+   see the files COPYING3 and COPYING.RUNTIME respectively.  If not, see
+   <http://www.gnu.org/licenses/>.  */
+
+/* Provide target-specific access to the futex system call.  */
+
+#ifndef SYS_futex
+#define SYS_futex 394
+#endif
+
+static inline void
+futex_wait (int *addr, int val)
+{
+  register long sc_0 __asm__("$0");
+  register long sc_16 __asm__("$16");
+  register long sc_17 __asm__("$17");
+  register long sc_18 __asm__("$18");
+  register long sc_19 __asm__("$19");
+
+  sc_0 = SYS_futex;
+  sc_16 = (long) addr;
+  sc_17 = gomp_futex_wait;
+  sc_18 = val;
+  sc_19 = 0;
+  __asm volatile ("callsys"
+		 : "=r"(sc_0), "=r"(sc_19)
+		 : "0"(sc_0), "r"(sc_16), "r"(sc_17), "r"(sc_18), "1"(sc_19)
+		 : "$1", "$2", "$3", "$4", "$5", "$6", "$7", "$8", "$22", "$23",
+		   "$24", "$25", "$27", "$28", "memory");
+  if (__builtin_expect (sc_19, 0) && sc_0 == ENOSYS)
+    {
+      gomp_futex_wait &= ~FUTEX_PRIVATE_FLAG;
+      gomp_futex_wake &= ~FUTEX_PRIVATE_FLAG;
+      sc_0 = SYS_futex;
+      sc_17 &= ~FUTEX_PRIVATE_FLAG;
+      sc_19 = 0;
+      __asm volatile ("callsys"
+		     : "=r"(sc_0), "=r"(sc_19)
+		     : "0"(sc_0), "r"(sc_16), "r"(sc_17), "r"(sc_18), "1"(sc_19)
+		     : "$1", "$2", "$3", "$4", "$5", "$6", "$7", "$8", "$22",
+		       "$23", "$24", "$25", "$27", "$28", "memory");
+    }
+}
+
+static inline void
+futex_wake (int *addr, int count)
+{
+  register long sc_0 __asm__("$0");
+  register long sc_16 __asm__("$16");
+  register long sc_17 __asm__("$17");
+  register long sc_18 __asm__("$18");
+  register long sc_19 __asm__("$19");
+
+  sc_0 = SYS_futex;
+  sc_16 = (long) addr;
+  sc_17 = gomp_futex_wake;
+  sc_18 = count;
+  __asm volatile ("callsys"
+		 : "=r"(sc_0), "=r"(sc_19)
+		 : "0"(sc_0), "r"(sc_16), "r"(sc_17), "r"(sc_18)
+		 : "$1", "$2", "$3", "$4", "$5", "$6", "$7", "$8", "$22", "$23",
+		   "$24", "$25", "$27", "$28", "memory");
+  if (__builtin_expect (sc_19, 0) && sc_0 == ENOSYS)
+    {
+      gomp_futex_wait &= ~FUTEX_PRIVATE_FLAG;
+      gomp_futex_wake &= ~FUTEX_PRIVATE_FLAG;
+      sc_0 = SYS_futex;
+      sc_17 &= ~FUTEX_PRIVATE_FLAG;
+      __asm volatile ("callsys"
+		     : "=r"(sc_0), "=r"(sc_19)
+		     : "0"(sc_0), "r"(sc_16), "r"(sc_17), "r"(sc_18)
+		     : "$1", "$2", "$3", "$4", "$5", "$6", "$7", "$8", "$22",
+		       "$23", "$24", "$25", "$27", "$28", "memory");
+    }
+}
+
+static inline void
+cpu_relax (void)
+{
+  __asm volatile ("" : : : "memory");
+}
diff --git a/libgomp/configure b/libgomp/configure
index 471c957b7..a1df23705 100755
--- a/libgomp/configure
+++ b/libgomp/configure
@@ -11841,6 +11841,12 @@ case `echo $GFORTRAN` in
       FC=no
     fi ;;
 esac
+case "${target}" in
+  sw_64-*-*)
+	FC="$GFORTRAN"
+	;;
+*)
+esac
 ac_ext=${ac_fc_srcext-f}
 ac_compile='$FC -c $FCFLAGS $ac_fcflags_srcext conftest.$ac_ext >&5'
 ac_link='$FC -o conftest$ac_exeext $FCFLAGS $LDFLAGS $ac_fcflags_srcext conftest.$ac_ext $LIBS >&5'
diff --git a/libgomp/configure.tgt b/libgomp/configure.tgt
index f924e9f98..a8023d0f2 100644
--- a/libgomp/configure.tgt
+++ b/libgomp/configure.tgt
@@ -87,6 +87,10 @@ if test x$enable_linux_futex = xyes; then
 	config_path="linux/s390 linux posix"
 	;;
 
+    sw_64*-*-linux*)
+	config_path="linux/sw_64 linux posix"
+	;;
+
     tile*-*-linux*)
 	config_path="linux/tile linux posix"
 	;;
diff --git a/libgomp/libgomp.spec.in b/libgomp/libgomp.spec.in
index 5651603f4..8442e6313 100644
--- a/libgomp/libgomp.spec.in
+++ b/libgomp/libgomp.spec.in
@@ -1,3 +1,3 @@
 # This spec file is read by gcc when linking.  It is used to specify the
 # standard libraries we need in order to link with libgomp.
-*link_gomp: @link_gomp@
+*link_gomp: @link_gomp@ --whole-archive -lpthread --no-whole-archive
-- 
2.25.1

