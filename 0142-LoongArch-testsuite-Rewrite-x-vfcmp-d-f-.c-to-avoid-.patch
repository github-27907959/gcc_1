From d568321f8894ed270bf0011892b86baa6d6b82bd Mon Sep 17 00:00:00 2001
From: Xi Ruoyao <xry111@xry111.site>
Date: Tue, 5 Mar 2024 20:46:57 +0800
Subject: [PATCH 142/188] LoongArch: testsuite: Rewrite {x,}vfcmp-{d,f}.c to
 avoid named registers

Loops on named vector register are not vectorized (see comment 11 of
PR113622), so the these test cases have been failing for a while.
Rewrite them using check-function-bodies to remove hard coding register
names.  A barrier is needed to always load the first operand before the
second operand.

gcc/testsuite/ChangeLog:

	* gcc.target/loongarch/vfcmp-f.c: Rewrite to avoid named
	registers.
	* gcc.target/loongarch/vfcmp-d.c: Likewise.
	* gcc.target/loongarch/xvfcmp-f.c: Likewise.
	* gcc.target/loongarch/xvfcmp-d.c: Likewise.
---
 gcc/testsuite/gcc.target/loongarch/vfcmp-d.c  | 202 ++++++++--
 gcc/testsuite/gcc.target/loongarch/vfcmp-f.c  | 347 ++++++++++++++----
 gcc/testsuite/gcc.target/loongarch/xvfcmp-d.c | 202 ++++++++--
 gcc/testsuite/gcc.target/loongarch/xvfcmp-f.c | 204 ++++++++--
 4 files changed, 816 insertions(+), 139 deletions(-)

diff --git a/gcc/testsuite/gcc.target/loongarch/vfcmp-d.c b/gcc/testsuite/gcc.target/loongarch/vfcmp-d.c
index 8b870ef38..87e4ed19e 100644
--- a/gcc/testsuite/gcc.target/loongarch/vfcmp-d.c
+++ b/gcc/testsuite/gcc.target/loongarch/vfcmp-d.c
@@ -1,28 +1,188 @@
 /* { dg-do compile } */
-/* { dg-options "-O2 -mlsx -ffixed-f0 -ffixed-f1 -ffixed-f2 -fno-vect-cost-model" } */
+/* { dg-options "-O2 -mlsx -fno-vect-cost-model" } */
+/* { dg-final { check-function-bodies "**" "" } } */
 
 #define F double
 #define I long long
 
 #include "vfcmp-f.c"
 
-/* { dg-final { scan-assembler "compare_quiet_equal:.*\tvfcmp\\.ceq\\.d\t\\\$vr2,\\\$vr0,\\\$vr1.*-compare_quiet_equal\n" } } */
-/* { dg-final { scan-assembler "compare_quiet_not_equal:.*\tvfcmp\\.cune\\.d\t\\\$vr2,\\\$vr0,\\\$vr1.*-compare_quiet_not_equal\n" } } */
-/* { dg-final { scan-assembler "compare_signaling_greater:.*\tvfcmp\\.slt\\.d\t\\\$vr2,\\\$vr1,\\\$vr0.*-compare_signaling_greater\n" } } */
-/* { dg-final { scan-assembler "compare_signaling_greater_equal:.*\tvfcmp\\.sle\\.d\t\\\$vr2,\\\$vr1,\\\$vr0.*-compare_signaling_greater_equal\n" } } */
-/* { dg-final { scan-assembler "compare_signaling_less:.*\tvfcmp\\.slt\\.d\t\\\$vr2,\\\$vr0,\\\$vr1.*-compare_signaling_less\n" } } */
-/* { dg-final { scan-assembler "compare_signaling_less_equal:.*\tvfcmp\\.sle\\.d\t\\\$vr2,\\\$vr0,\\\$vr1.*-compare_signaling_less_equal\n" } } */
-/* { dg-final { scan-assembler "compare_signaling_not_greater:.*\tvfcmp\\.sule\\.d\t\\\$vr2,\\\$vr0,\\\$vr1.*-compare_signaling_not_greater\n" } } */
-/* { dg-final { scan-assembler "compare_signaling_less_unordered:.*\tvfcmp\\.sult\\.d\t\\\$vr2,\\\$vr0,\\\$vr1.*-compare_signaling_less_unordered\n" } } */
-/* { dg-final { scan-assembler "compare_signaling_not_less:.*\tvfcmp\\.sule\\.d\t\\\$vr2,\\\$vr1,\\\$vr0.*-compare_signaling_not_less\n" } } */
-/* { dg-final { scan-assembler "compare_signaling_greater_unordered:.*\tvfcmp\\.sult\\.d\t\\\$vr2,\\\$vr1,\\\$vr0.*-compare_signaling_greater_unordered\n" } } */
-/* { dg-final { scan-assembler "compare_quiet_less:.*\tvfcmp\\.clt\\.d\t\\\$vr2,\\\$vr0,\\\$vr1.*-compare_quiet_less\n" } } */
-/* { dg-final { scan-assembler "compare_quiet_less_equal:.*\tvfcmp\\.cle\\.d\t\\\$vr2,\\\$vr0,\\\$vr1.*-compare_quiet_less_equal\n" } } */
-/* { dg-final { scan-assembler "compare_quiet_greater:.*\tvfcmp\\.clt\\.d\t\\\$vr2,\\\$vr1,\\\$vr0.*-compare_quiet_greater\n" } } */
-/* { dg-final { scan-assembler "compare_quiet_greater_equal:.*\tvfcmp\\.cle\\.d\t\\\$vr2,\\\$vr1,\\\$vr0.*-compare_quiet_greater_equal\n" } } */
-/* { dg-final { scan-assembler "compare_quiet_not_less:.*\tvfcmp\\.cule\\.d\t\\\$vr2,\\\$vr1,\\\$vr0.*-compare_quiet_not_less\n" } } */
-/* { dg-final { scan-assembler "compare_quiet_greater_unordered:.*\tvfcmp\\.cult\\.d\t\\\$vr2,\\\$vr1,\\\$vr0.*-compare_quiet_greater_unordered\n" } } */
-/* { dg-final { scan-assembler "compare_quiet_not_greater:.*\tvfcmp\\.cule\\.d\t\\\$vr2,\\\$vr0,\\\$vr1.*-compare_quiet_not_greater\n" } } */
-/* { dg-final { scan-assembler "compare_quiet_less_unordered:.*\tvfcmp\\.cult\\.d\t\\\$vr2,\\\$vr0,\\\$vr1.*-compare_quiet_less_unordered\n" } } */
-/* { dg-final { scan-assembler "compare_quiet_unordered:.*\tvfcmp\\.cun\\.d\t\\\$vr2,\\\$vr0,\\\$vr1.*-compare_quiet_unordered\n" } } */
-/* { dg-final { scan-assembler "compare_quiet_ordered:.*\tvfcmp\\.cor\\.d\t\\\$vr2,\\\$vr0,\\\$vr1.*-compare_quiet_ordered\n" } } */
+/*
+** compare_quiet_equal:
+** 	vld	(\$vr[0-9]+),\$r4,0
+** 	vld	(\$vr[0-9]+),\$r5,0
+** 	vfcmp.ceq.d	(\$vr[0-9]+),(\1,\2|\2,\1)
+**	vst	\3,\$r6,0
+**	jr	\$r1
+*/
+
+/*
+** compare_quiet_not_equal:
+** 	vld	(\$vr[0-9]+),\$r4,0
+** 	vld	(\$vr[0-9]+),\$r5,0
+** 	vfcmp.cune.d	(\$vr[0-9]+),(\1,\2|\2,\1)
+**	vst	\3,\$r6,0
+**	jr	\$r1
+*/
+
+/*
+** compare_signaling_greater:
+** 	vld	(\$vr[0-9]+),\$r4,0
+** 	vld	(\$vr[0-9]+),\$r5,0
+** 	vfcmp.slt.d	(\$vr[0-9]+),\2,\1
+**	vst	\3,\$r6,0
+**	jr	\$r1
+*/
+
+/*
+** compare_signaling_greater_equal:
+** 	vld	(\$vr[0-9]+),\$r4,0
+** 	vld	(\$vr[0-9]+),\$r5,0
+** 	vfcmp.sle.d	(\$vr[0-9]+),\2,\1
+**	vst	\3,\$r6,0
+**	jr	\$r1
+*/
+
+/*
+** compare_signaling_less:
+** 	vld	(\$vr[0-9]+),\$r4,0
+** 	vld	(\$vr[0-9]+),\$r5,0
+** 	vfcmp.slt.d	(\$vr[0-9]+),\1,\2
+**	vst	\3,\$r6,0
+**	jr	\$r1
+*/
+
+/*
+** compare_signaling_less_equal:
+** 	vld	(\$vr[0-9]+),\$r4,0
+** 	vld	(\$vr[0-9]+),\$r5,0
+** 	vfcmp.sle.d	(\$vr[0-9]+),\1,\2
+**	vst	\3,\$r6,0
+**	jr	\$r1
+*/
+
+/*
+** compare_signaling_not_greater:
+** 	vld	(\$vr[0-9]+),\$r4,0
+** 	vld	(\$vr[0-9]+),\$r5,0
+** 	vfcmp.sule.d	(\$vr[0-9]+),\1,\2
+**	vst	\3,\$r6,0
+**	jr	\$r1
+*/
+
+/*
+** compare_signaling_less_unordered:
+** 	vld	(\$vr[0-9]+),\$r4,0
+** 	vld	(\$vr[0-9]+),\$r5,0
+** 	vfcmp.sult.d	(\$vr[0-9]+),\1,\2
+**	vst	\3,\$r6,0
+**	jr	\$r1
+*/
+
+/*
+** compare_signaling_not_less:
+** 	vld	(\$vr[0-9]+),\$r4,0
+** 	vld	(\$vr[0-9]+),\$r5,0
+** 	vfcmp.sule.d	(\$vr[0-9]+),\2,\1
+**	vst	\3,\$r6,0
+**	jr	\$r1
+*/
+
+/*
+** compare_signaling_greater_unordered:
+** 	vld	(\$vr[0-9]+),\$r4,0
+** 	vld	(\$vr[0-9]+),\$r5,0
+** 	vfcmp.sult.d	(\$vr[0-9]+),\2,\1
+**	vst	\3,\$r6,0
+**	jr	\$r1
+*/
+
+/*
+** compare_quiet_less:
+** 	vld	(\$vr[0-9]+),\$r4,0
+** 	vld	(\$vr[0-9]+),\$r5,0
+** 	vfcmp.clt.d	(\$vr[0-9]+),\1,\2
+**	vst	\3,\$r6,0
+**	jr	\$r1
+*/
+
+/*
+** compare_quiet_less_equal:
+** 	vld	(\$vr[0-9]+),\$r4,0
+** 	vld	(\$vr[0-9]+),\$r5,0
+** 	vfcmp.cle.d	(\$vr[0-9]+),\1,\2
+**	vst	\3,\$r6,0
+**	jr	\$r1
+*/
+
+/*
+** compare_quiet_greater:
+** 	vld	(\$vr[0-9]+),\$r4,0
+** 	vld	(\$vr[0-9]+),\$r5,0
+** 	vfcmp.clt.d	(\$vr[0-9]+),\2,\1
+**	vst	\3,\$r6,0
+**	jr	\$r1
+*/
+
+/*
+** compare_quiet_greater_equal:
+** 	vld	(\$vr[0-9]+),\$r4,0
+** 	vld	(\$vr[0-9]+),\$r5,0
+** 	vfcmp.cle.d	(\$vr[0-9]+),\2,\1
+**	vst	\3,\$r6,0
+**	jr	\$r1
+*/
+
+/*
+** compare_quiet_not_less:
+** 	vld	(\$vr[0-9]+),\$r4,0
+** 	vld	(\$vr[0-9]+),\$r5,0
+** 	vfcmp.cule.d	(\$vr[0-9]+),\2,\1
+**	vst	\3,\$r6,0
+**	jr	\$r1
+*/
+
+/*
+** compare_quiet_greater_unordered:
+** 	vld	(\$vr[0-9]+),\$r4,0
+** 	vld	(\$vr[0-9]+),\$r5,0
+** 	vfcmp.cult.d	(\$vr[0-9]+),\2,\1
+**	vst	\3,\$r6,0
+**	jr	\$r1
+*/
+
+/*
+** compare_quiet_not_greater:
+** 	vld	(\$vr[0-9]+),\$r4,0
+** 	vld	(\$vr[0-9]+),\$r5,0
+** 	vfcmp.cule.d	(\$vr[0-9]+),\1,\2
+**	vst	\3,\$r6,0
+**	jr	\$r1
+*/
+
+/*
+** compare_quiet_less_unordered:
+** 	vld	(\$vr[0-9]+),\$r4,0
+** 	vld	(\$vr[0-9]+),\$r5,0
+** 	vfcmp.cult.d	(\$vr[0-9]+),\1,\2
+**	vst	\3,\$r6,0
+**	jr	\$r1
+*/
+
+/*
+** compare_quiet_unordered:
+** 	vld	(\$vr[0-9]+),\$r4,0
+** 	vld	(\$vr[0-9]+),\$r5,0
+** 	vfcmp.cun.d	(\$vr[0-9]+),(\1,\2|\2,\1)
+**	vst	\3,\$r6,0
+**	jr	\$r1
+*/
+
+/*
+** compare_quiet_ordered:
+** 	vld	(\$vr[0-9]+),\$r4,0
+** 	vld	(\$vr[0-9]+),\$r5,0
+** 	vfcmp.cor.d	(\$vr[0-9]+),(\1,\2|\2,\1)
+**	vst	\3,\$r6,0
+**	jr	\$r1
+*/
diff --git a/gcc/testsuite/gcc.target/loongarch/vfcmp-f.c b/gcc/testsuite/gcc.target/loongarch/vfcmp-f.c
index b9110b90c..8d2671998 100644
--- a/gcc/testsuite/gcc.target/loongarch/vfcmp-f.c
+++ b/gcc/testsuite/gcc.target/loongarch/vfcmp-f.c
@@ -2,7 +2,8 @@
    For details read C23 Annex F.3 and LoongArch Vol. 1 section 3.2.2.1.  */
 
 /* { dg-do compile } */
-/* { dg-options "-O2 -mlsx -ffixed-f0 -ffixed-f1 -ffixed-f2 -fno-vect-cost-model" } */
+/* { dg-options "-O2 -mlsx -fno-vect-cost-model" } */
+/* { dg-final { check-function-bodies "**" "" } } */
 
 #ifndef F
 #define F float
@@ -19,160 +20,354 @@
 typedef F VF __attribute__ ((vector_size (VL)));
 typedef I VI __attribute__ ((vector_size (VL)));
 
-register VF a asm ("f0");
-register VF b asm ("f1");
-register VI c asm ("f2");
+#define ARGS const VF *a, const VF *b, VI *c
 
 void
-compare_quiet_equal (void)
+compare_quiet_equal (ARGS)
 {
-  c = (a == b);
+  VF _a = *a;
+  asm("" ::: "memory");
+  *c = (_a == *b);
 }
 
 void
-compare_quiet_not_equal (void)
+compare_quiet_not_equal (ARGS)
 {
-  c = (a != b);
+  VF _a = *a;
+  asm("" ::: "memory");
+  *c = (_a != *b);
 }
 
 void
-compare_signaling_greater (void)
+compare_signaling_greater (ARGS)
 {
-  c = (a > b);
+  VF _a = *a;
+  asm("" ::: "memory");
+  *c = (_a > *b);
 }
 
 void
-compare_signaling_greater_equal (void)
+compare_signaling_greater_equal (ARGS)
 {
-  c = (a >= b);
+  VF _a = *a;
+  asm("" ::: "memory");
+  *c = (_a >= *b);
 }
 
 void
-compare_signaling_less (void)
+compare_signaling_less (ARGS)
 {
-  c = (a < b);
+  VF _a = *a;
+  asm("" ::: "memory");
+  *c = (_a < *b);
 }
 
 void
-compare_signaling_less_equal (void)
+compare_signaling_less_equal (ARGS)
 {
-  c = (a <= b);
+  VF _a = *a;
+  asm("" ::: "memory");
+  *c = (_a <= *b);
 }
 
 void
-compare_signaling_not_greater (void)
+compare_signaling_not_greater (ARGS)
 {
-  c = ~(a > b);
+  VF _a = *a;
+  asm("" ::: "memory");
+  *c = ~(_a > *b);
 }
 
 void
-compare_signaling_less_unordered (void)
+compare_signaling_less_unordered (ARGS)
 {
-  c = ~(a >= b);
+  VF _a = *a;
+  asm("" ::: "memory");
+  *c = ~(_a >= *b);
 }
 
 void
-compare_signaling_not_less (void)
+compare_signaling_not_less (ARGS)
 {
-  c = ~(a < b);
+  VF _a = *a;
+  asm("" ::: "memory");
+  *c = ~(_a < *b);
 }
 
 void
-compare_signaling_greater_unordered (void)
+compare_signaling_greater_unordered (ARGS)
 {
-  c = ~(a <= b);
+  VF _a = *a;
+  asm("" ::: "memory");
+  *c = ~(_a <= *b);
 }
 
 void
-compare_quiet_less (void)
+compare_quiet_less (ARGS)
 {
-  for (int i = 0; i < sizeof (c) / sizeof (c[0]); i++)
-    c[i] = __builtin_isless (a[i], b[i]) ? -1 : 0;
+  VF _a = *a;
+  asm("" ::: "memory");
+  for (int i = 0; i < sizeof (*c) / sizeof ((*c)[0]); i++)
+    (*c)[i] = __builtin_isless (_a[i], (*b)[i]) ? -1 : 0;
 }
 
 void
-compare_quiet_less_equal (void)
+compare_quiet_less_equal (ARGS)
 {
-  for (int i = 0; i < sizeof (c) / sizeof (c[0]); i++)
-    c[i] = __builtin_islessequal (a[i], b[i]) ? -1 : 0;
+  VF _a = *a;
+  asm("" ::: "memory");
+  for (int i = 0; i < sizeof (*c) / sizeof ((*c)[0]); i++)
+    (*c)[i] = __builtin_islessequal (_a[i], (*b)[i]) ? -1 : 0;
 }
 
 void
-compare_quiet_greater (void)
+compare_quiet_greater (ARGS)
 {
-  for (int i = 0; i < sizeof (c) / sizeof (c[0]); i++)
-    c[i] = __builtin_isgreater (a[i], b[i]) ? -1 : 0;
+  VF _a = *a;
+  asm("" ::: "memory");
+  for (int i = 0; i < sizeof (*c) / sizeof ((*c)[0]); i++)
+    (*c)[i] = __builtin_isgreater (_a[i], (*b)[i]) ? -1 : 0;
 }
 
 void
-compare_quiet_greater_equal (void)
+compare_quiet_greater_equal (ARGS)
 {
-  for (int i = 0; i < sizeof (c) / sizeof (c[0]); i++)
-    c[i] = __builtin_isgreaterequal (a[i], b[i]) ? -1 : 0;
+  VF _a = *a;
+  asm("" ::: "memory");
+  for (int i = 0; i < sizeof (*c) / sizeof ((*c)[0]); i++)
+    (*c)[i] = __builtin_isgreaterequal (_a[i], (*b)[i]) ? -1 : 0;
 }
 
 void
-compare_quiet_not_less (void)
+compare_quiet_not_less (ARGS)
 {
-  for (int i = 0; i < sizeof (c) / sizeof (c[0]); i++)
-    c[i] = __builtin_isless (a[i], b[i]) ? 0 : -1;
+  VF _a = *a;
+  asm("" ::: "memory");
+  for (int i = 0; i < sizeof (*c) / sizeof ((*c)[0]); i++)
+    (*c)[i] = __builtin_isless (_a[i], (*b)[i]) ? 0 : -1;
 }
 
 void
-compare_quiet_greater_unordered (void)
+compare_quiet_greater_unordered (ARGS)
 {
-  for (int i = 0; i < sizeof (c) / sizeof (c[0]); i++)
-    c[i] = __builtin_islessequal (a[i], b[i]) ? 0 : -1;
+  VF _a = *a;
+  asm("" ::: "memory");
+  for (int i = 0; i < sizeof (*c) / sizeof ((*c)[0]); i++)
+    (*c)[i] = __builtin_islessequal (_a[i], (*b)[i]) ? 0 : -1;
 }
 
 void
-compare_quiet_not_greater (void)
+compare_quiet_not_greater (ARGS)
 {
-  for (int i = 0; i < sizeof (c) / sizeof (c[0]); i++)
-    c[i] = __builtin_isgreater (a[i], b[i]) ? 0 : -1;
+  VF _a = *a;
+  asm("" ::: "memory");
+  for (int i = 0; i < sizeof (*c) / sizeof ((*c)[0]); i++)
+    (*c)[i] = __builtin_isgreater (_a[i], (*b)[i]) ? 0 : -1;
 }
 
 void
-compare_quiet_less_unordered (void)
+compare_quiet_less_unordered (ARGS)
 {
-  for (int i = 0; i < sizeof (c) / sizeof (c[0]); i++)
-    c[i] = __builtin_isgreaterequal (a[i], b[i]) ? 0 : -1;
+  VF _a = *a;
+  asm("" ::: "memory");
+  for (int i = 0; i < sizeof (*c) / sizeof ((*c)[0]); i++)
+    (*c)[i] = __builtin_isgreaterequal (_a[i], (*b)[i]) ? 0 : -1;
 }
 
 void
-compare_quiet_unordered (void)
+compare_quiet_unordered (ARGS)
 {
-  for (int i = 0; i < sizeof (c) / sizeof (c[0]); i++)
-    c[i] = __builtin_isunordered (a[i], b[i]) ? -1 : 0;
+  VF _a = *a;
+  asm("" ::: "memory");
+  for (int i = 0; i < sizeof (*c) / sizeof ((*c)[0]); i++)
+    (*c)[i] = __builtin_isunordered (_a[i], (*b)[i]) ? -1 : 0;
 }
 
 void
-compare_quiet_ordered (void)
+compare_quiet_ordered (ARGS)
 {
-  for (int i = 0; i < sizeof (c) / sizeof (c[0]); i++)
-    c[i] = __builtin_isunordered (a[i], b[i]) ? 0 : -1;
+  VF _a = *a;
+  asm("" ::: "memory");
+  for (int i = 0; i < sizeof (*c) / sizeof ((*c)[0]); i++)
+    (*c)[i] = __builtin_isunordered (_a[i], (*b)[i]) ? 0 : -1;
 }
 
-/* The "-<function_name>" matches the .size directive after the function
-   body, so we can ensure the instruction is in the correct function.  */
+/*
+** compare_quiet_equal:
+** 	vld	(\$vr[0-9]+),\$r4,0
+** 	vld	(\$vr[0-9]+),\$r5,0
+** 	vfcmp.ceq.s	(\$vr[0-9]+),(\1,\2|\2,\1)
+**	vst	\3,\$r6,0
+**	jr	\$r1
+*/
 
-/* { dg-final { scan-assembler "compare_quiet_equal:.*\tvfcmp\\.ceq\\.s\t\\\$vr2,\\\$vr0,\\\$vr1.*-compare_quiet_equal\n" } } */
-/* { dg-final { scan-assembler "compare_quiet_not_equal:.*\tvfcmp\\.cune\\.s\t\\\$vr2,\\\$vr0,\\\$vr1.*-compare_quiet_not_equal\n" } } */
-/* { dg-final { scan-assembler "compare_signaling_greater:.*\tvfcmp\\.slt\\.s\t\\\$vr2,\\\$vr1,\\\$vr0.*-compare_signaling_greater\n" } } */
-/* { dg-final { scan-assembler "compare_signaling_greater_equal:.*\tvfcmp\\.sle\\.s\t\\\$vr2,\\\$vr1,\\\$vr0.*-compare_signaling_greater_equal\n" } } */
-/* { dg-final { scan-assembler "compare_signaling_less:.*\tvfcmp\\.slt\\.s\t\\\$vr2,\\\$vr0,\\\$vr1.*-compare_signaling_less\n" } } */
-/* { dg-final { scan-assembler "compare_signaling_less_equal:.*\tvfcmp\\.sle\\.s\t\\\$vr2,\\\$vr0,\\\$vr1.*-compare_signaling_less_equal\n" } } */
-/* { dg-final { scan-assembler "compare_signaling_not_greater:.*\tvfcmp\\.sule\\.s\t\\\$vr2,\\\$vr0,\\\$vr1.*-compare_signaling_not_greater\n" } } */
-/* { dg-final { scan-assembler "compare_signaling_less_unordered:.*\tvfcmp\\.sult\\.s\t\\\$vr2,\\\$vr0,\\\$vr1.*-compare_signaling_less_unordered\n" } } */
-/* { dg-final { scan-assembler "compare_signaling_not_less:.*\tvfcmp\\.sule\\.s\t\\\$vr2,\\\$vr1,\\\$vr0.*-compare_signaling_not_less\n" } } */
-/* { dg-final { scan-assembler "compare_signaling_greater_unordered:.*\tvfcmp\\.sult\\.s\t\\\$vr2,\\\$vr1,\\\$vr0.*-compare_signaling_greater_unordered\n" } } */
-/* { dg-final { scan-assembler "compare_quiet_less:.*\tvfcmp\\.clt\\.s\t\\\$vr2,\\\$vr0,\\\$vr1.*-compare_quiet_less\n" } } */
-/* { dg-final { scan-assembler "compare_quiet_less_equal:.*\tvfcmp\\.cle\\.s\t\\\$vr2,\\\$vr0,\\\$vr1.*-compare_quiet_less_equal\n" } } */
-/* { dg-final { scan-assembler "compare_quiet_greater:.*\tvfcmp\\.clt\\.s\t\\\$vr2,\\\$vr1,\\\$vr0.*-compare_quiet_greater\n" } } */
-/* { dg-final { scan-assembler "compare_quiet_greater_equal:.*\tvfcmp\\.cle\\.s\t\\\$vr2,\\\$vr1,\\\$vr0.*-compare_quiet_greater_equal\n" } } */
-/* { dg-final { scan-assembler "compare_quiet_not_less:.*\tvfcmp\\.cule\\.s\t\\\$vr2,\\\$vr1,\\\$vr0.*-compare_quiet_not_less\n" } } */
-/* { dg-final { scan-assembler "compare_quiet_greater_unordered:.*\tvfcmp\\.cult\\.s\t\\\$vr2,\\\$vr1,\\\$vr0.*-compare_quiet_greater_unordered\n" } } */
-/* { dg-final { scan-assembler "compare_quiet_not_greater:.*\tvfcmp\\.cule\\.s\t\\\$vr2,\\\$vr0,\\\$vr1.*-compare_quiet_not_greater\n" } } */
-/* { dg-final { scan-assembler "compare_quiet_less_unordered:.*\tvfcmp\\.cult\\.s\t\\\$vr2,\\\$vr0,\\\$vr1.*-compare_quiet_less_unordered\n" } } */
-/* { dg-final { scan-assembler "compare_quiet_unordered:.*\tvfcmp\\.cun\\.s\t\\\$vr2,\\\$vr0,\\\$vr1.*-compare_quiet_unordered\n" } } */
-/* { dg-final { scan-assembler "compare_quiet_ordered:.*\tvfcmp\\.cor\\.s\t\\\$vr2,\\\$vr0,\\\$vr1.*-compare_quiet_ordered\n" } } */
+/*
+** compare_quiet_not_equal:
+** 	vld	(\$vr[0-9]+),\$r4,0
+** 	vld	(\$vr[0-9]+),\$r5,0
+** 	vfcmp.cune.s	(\$vr[0-9]+),(\1,\2|\2,\1)
+**	vst	\3,\$r6,0
+**	jr	\$r1
+*/
+
+/*
+** compare_signaling_greater:
+** 	vld	(\$vr[0-9]+),\$r4,0
+** 	vld	(\$vr[0-9]+),\$r5,0
+** 	vfcmp.slt.s	(\$vr[0-9]+),\2,\1
+**	vst	\3,\$r6,0
+**	jr	\$r1
+*/
+
+/*
+** compare_signaling_greater_equal:
+** 	vld	(\$vr[0-9]+),\$r4,0
+** 	vld	(\$vr[0-9]+),\$r5,0
+** 	vfcmp.sle.s	(\$vr[0-9]+),\2,\1
+**	vst	\3,\$r6,0
+**	jr	\$r1
+*/
+
+/*
+** compare_signaling_less:
+** 	vld	(\$vr[0-9]+),\$r4,0
+** 	vld	(\$vr[0-9]+),\$r5,0
+** 	vfcmp.slt.s	(\$vr[0-9]+),\1,\2
+**	vst	\3,\$r6,0
+**	jr	\$r1
+*/
+
+/*
+** compare_signaling_less_equal:
+** 	vld	(\$vr[0-9]+),\$r4,0
+** 	vld	(\$vr[0-9]+),\$r5,0
+** 	vfcmp.sle.s	(\$vr[0-9]+),\1,\2
+**	vst	\3,\$r6,0
+**	jr	\$r1
+*/
+
+/*
+** compare_signaling_not_greater:
+** 	vld	(\$vr[0-9]+),\$r4,0
+** 	vld	(\$vr[0-9]+),\$r5,0
+** 	vfcmp.sule.s	(\$vr[0-9]+),\1,\2
+**	vst	\3,\$r6,0
+**	jr	\$r1
+*/
+
+/*
+** compare_signaling_less_unordered:
+** 	vld	(\$vr[0-9]+),\$r4,0
+** 	vld	(\$vr[0-9]+),\$r5,0
+** 	vfcmp.sult.s	(\$vr[0-9]+),\1,\2
+**	vst	\3,\$r6,0
+**	jr	\$r1
+*/
+
+/*
+** compare_signaling_not_less:
+** 	vld	(\$vr[0-9]+),\$r4,0
+** 	vld	(\$vr[0-9]+),\$r5,0
+** 	vfcmp.sule.s	(\$vr[0-9]+),\2,\1
+**	vst	\3,\$r6,0
+**	jr	\$r1
+*/
+
+/*
+** compare_signaling_greater_unordered:
+** 	vld	(\$vr[0-9]+),\$r4,0
+** 	vld	(\$vr[0-9]+),\$r5,0
+** 	vfcmp.sult.s	(\$vr[0-9]+),\2,\1
+**	vst	\3,\$r6,0
+**	jr	\$r1
+*/
+
+/*
+** compare_quiet_less:
+** 	vld	(\$vr[0-9]+),\$r4,0
+** 	vld	(\$vr[0-9]+),\$r5,0
+** 	vfcmp.clt.s	(\$vr[0-9]+),\1,\2
+**	vst	\3,\$r6,0
+**	jr	\$r1
+*/
+
+/*
+** compare_quiet_less_equal:
+** 	vld	(\$vr[0-9]+),\$r4,0
+** 	vld	(\$vr[0-9]+),\$r5,0
+** 	vfcmp.cle.s	(\$vr[0-9]+),\1,\2
+**	vst	\3,\$r6,0
+**	jr	\$r1
+*/
+
+/*
+** compare_quiet_greater:
+** 	vld	(\$vr[0-9]+),\$r4,0
+** 	vld	(\$vr[0-9]+),\$r5,0
+** 	vfcmp.clt.s	(\$vr[0-9]+),\2,\1
+**	vst	\3,\$r6,0
+**	jr	\$r1
+*/
+
+/*
+** compare_quiet_greater_equal:
+** 	vld	(\$vr[0-9]+),\$r4,0
+** 	vld	(\$vr[0-9]+),\$r5,0
+** 	vfcmp.cle.s	(\$vr[0-9]+),\2,\1
+**	vst	\3,\$r6,0
+**	jr	\$r1
+*/
+
+/*
+** compare_quiet_not_less:
+** 	vld	(\$vr[0-9]+),\$r4,0
+** 	vld	(\$vr[0-9]+),\$r5,0
+** 	vfcmp.cule.s	(\$vr[0-9]+),\2,\1
+**	vst	\3,\$r6,0
+**	jr	\$r1
+*/
+
+/*
+** compare_quiet_greater_unordered:
+** 	vld	(\$vr[0-9]+),\$r4,0
+** 	vld	(\$vr[0-9]+),\$r5,0
+** 	vfcmp.cult.s	(\$vr[0-9]+),\2,\1
+**	vst	\3,\$r6,0
+**	jr	\$r1
+*/
+
+/*
+** compare_quiet_not_greater:
+** 	vld	(\$vr[0-9]+),\$r4,0
+** 	vld	(\$vr[0-9]+),\$r5,0
+** 	vfcmp.cule.s	(\$vr[0-9]+),\1,\2
+**	vst	\3,\$r6,0
+**	jr	\$r1
+*/
+
+/*
+** compare_quiet_less_unordered:
+** 	vld	(\$vr[0-9]+),\$r4,0
+** 	vld	(\$vr[0-9]+),\$r5,0
+** 	vfcmp.cult.s	(\$vr[0-9]+),\1,\2
+**	vst	\3,\$r6,0
+**	jr	\$r1
+*/
+
+/*
+** compare_quiet_unordered:
+** 	vld	(\$vr[0-9]+),\$r4,0
+** 	vld	(\$vr[0-9]+),\$r5,0
+** 	vfcmp.cun.s	(\$vr[0-9]+),(\1,\2|\2,\1)
+**	vst	\3,\$r6,0
+**	jr	\$r1
+*/
+
+/*
+** compare_quiet_ordered:
+** 	vld	(\$vr[0-9]+),\$r4,0
+** 	vld	(\$vr[0-9]+),\$r5,0
+** 	vfcmp.cor.s	(\$vr[0-9]+),(\1,\2|\2,\1)
+**	vst	\3,\$r6,0
+**	jr	\$r1
+*/
diff --git a/gcc/testsuite/gcc.target/loongarch/xvfcmp-d.c b/gcc/testsuite/gcc.target/loongarch/xvfcmp-d.c
index d8017caaa..b27efebad 100644
--- a/gcc/testsuite/gcc.target/loongarch/xvfcmp-d.c
+++ b/gcc/testsuite/gcc.target/loongarch/xvfcmp-d.c
@@ -1,5 +1,6 @@
 /* { dg-do compile } */
-/* { dg-options "-O2 -mlasx -ffixed-f0 -ffixed-f1 -ffixed-f2 -fno-vect-cost-model" } */
+/* { dg-options "-O2 -mlasx -fno-vect-cost-model" } */
+/* { dg-final { check-function-bodies "**" "" } } */
 
 #define F double
 #define I long long
@@ -7,23 +8,182 @@
 
 #include "vfcmp-f.c"
 
-/* { dg-final { scan-assembler "compare_quiet_equal:.*\txvfcmp\\.ceq\\.d\t\\\$xr2,\\\$xr0,\\\$xr1.*-compare_quiet_equal\n" } } */
-/* { dg-final { scan-assembler "compare_quiet_not_equal:.*\txvfcmp\\.cune\\.d\t\\\$xr2,\\\$xr0,\\\$xr1.*-compare_quiet_not_equal\n" } } */
-/* { dg-final { scan-assembler "compare_signaling_greater:.*\txvfcmp\\.slt\\.d\t\\\$xr2,\\\$xr1,\\\$xr0.*-compare_signaling_greater\n" } } */
-/* { dg-final { scan-assembler "compare_signaling_greater_equal:.*\txvfcmp\\.sle\\.d\t\\\$xr2,\\\$xr1,\\\$xr0.*-compare_signaling_greater_equal\n" } } */
-/* { dg-final { scan-assembler "compare_signaling_less:.*\txvfcmp\\.slt\\.d\t\\\$xr2,\\\$xr0,\\\$xr1.*-compare_signaling_less\n" } } */
-/* { dg-final { scan-assembler "compare_signaling_less_equal:.*\txvfcmp\\.sle\\.d\t\\\$xr2,\\\$xr0,\\\$xr1.*-compare_signaling_less_equal\n" } } */
-/* { dg-final { scan-assembler "compare_signaling_not_greater:.*\txvfcmp\\.sule\\.d\t\\\$xr2,\\\$xr0,\\\$xr1.*-compare_signaling_not_greater\n" } } */
-/* { dg-final { scan-assembler "compare_signaling_less_unordered:.*\txvfcmp\\.sult\\.d\t\\\$xr2,\\\$xr0,\\\$xr1.*-compare_signaling_less_unordered\n" } } */
-/* { dg-final { scan-assembler "compare_signaling_not_less:.*\txvfcmp\\.sule\\.d\t\\\$xr2,\\\$xr1,\\\$xr0.*-compare_signaling_not_less\n" } } */
-/* { dg-final { scan-assembler "compare_signaling_greater_unordered:.*\txvfcmp\\.sult\\.d\t\\\$xr2,\\\$xr1,\\\$xr0.*-compare_signaling_greater_unordered\n" } } */
-/* { dg-final { scan-assembler "compare_quiet_less:.*\txvfcmp\\.clt\\.d\t\\\$xr2,\\\$xr0,\\\$xr1.*-compare_quiet_less\n" } } */
-/* { dg-final { scan-assembler "compare_quiet_less_equal:.*\txvfcmp\\.cle\\.d\t\\\$xr2,\\\$xr0,\\\$xr1.*-compare_quiet_less_equal\n" } } */
-/* { dg-final { scan-assembler "compare_quiet_greater:.*\txvfcmp\\.clt\\.d\t\\\$xr2,\\\$xr1,\\\$xr0.*-compare_quiet_greater\n" } } */
-/* { dg-final { scan-assembler "compare_quiet_greater_equal:.*\txvfcmp\\.cle\\.d\t\\\$xr2,\\\$xr1,\\\$xr0.*-compare_quiet_greater_equal\n" } } */
-/* { dg-final { scan-assembler "compare_quiet_not_less:.*\txvfcmp\\.cule\\.d\t\\\$xr2,\\\$xr1,\\\$xr0.*-compare_quiet_not_less\n" } } */
-/* { dg-final { scan-assembler "compare_quiet_greater_unordered:.*\txvfcmp\\.cult\\.d\t\\\$xr2,\\\$xr1,\\\$xr0.*-compare_quiet_greater_unordered\n" } } */
-/* { dg-final { scan-assembler "compare_quiet_not_greater:.*\txvfcmp\\.cule\\.d\t\\\$xr2,\\\$xr0,\\\$xr1.*-compare_quiet_not_greater\n" } } */
-/* { dg-final { scan-assembler "compare_quiet_less_unordered:.*\txvfcmp\\.cult\\.d\t\\\$xr2,\\\$xr0,\\\$xr1.*-compare_quiet_less_unordered\n" } } */
-/* { dg-final { scan-assembler "compare_quiet_unordered:.*\txvfcmp\\.cun\\.d\t\\\$xr2,\\\$xr0,\\\$xr1.*-compare_quiet_unordered\n" } } */
-/* { dg-final { scan-assembler "compare_quiet_ordered:.*\txvfcmp\\.cor\\.d\t\\\$xr2,\\\$xr0,\\\$xr1.*-compare_quiet_ordered\n" } } */
+/*
+** compare_quiet_equal:
+** 	xvld	(\$xr[0-9]+),\$r4,0
+** 	xvld	(\$xr[0-9]+),\$r5,0
+** 	xvfcmp.ceq.d	(\$xr[0-9]+),(\1,\2|\2,\1)
+**	xvst	\3,\$r6,0
+**	jr	\$r1
+*/
+
+/*
+** compare_quiet_not_equal:
+** 	xvld	(\$xr[0-9]+),\$r4,0
+** 	xvld	(\$xr[0-9]+),\$r5,0
+** 	xvfcmp.cune.d	(\$xr[0-9]+),(\1,\2|\2,\1)
+**	xvst	\3,\$r6,0
+**	jr	\$r1
+*/
+
+/*
+** compare_signaling_greater:
+** 	xvld	(\$xr[0-9]+),\$r4,0
+** 	xvld	(\$xr[0-9]+),\$r5,0
+** 	xvfcmp.slt.d	(\$xr[0-9]+),\2,\1
+**	xvst	\3,\$r6,0
+**	jr	\$r1
+*/
+
+/*
+** compare_signaling_greater_equal:
+** 	xvld	(\$xr[0-9]+),\$r4,0
+** 	xvld	(\$xr[0-9]+),\$r5,0
+** 	xvfcmp.sle.d	(\$xr[0-9]+),\2,\1
+**	xvst	\3,\$r6,0
+**	jr	\$r1
+*/
+
+/*
+** compare_signaling_less:
+** 	xvld	(\$xr[0-9]+),\$r4,0
+** 	xvld	(\$xr[0-9]+),\$r5,0
+** 	xvfcmp.slt.d	(\$xr[0-9]+),\1,\2
+**	xvst	\3,\$r6,0
+**	jr	\$r1
+*/
+
+/*
+** compare_signaling_less_equal:
+** 	xvld	(\$xr[0-9]+),\$r4,0
+** 	xvld	(\$xr[0-9]+),\$r5,0
+** 	xvfcmp.sle.d	(\$xr[0-9]+),\1,\2
+**	xvst	\3,\$r6,0
+**	jr	\$r1
+*/
+
+/*
+** compare_signaling_not_greater:
+** 	xvld	(\$xr[0-9]+),\$r4,0
+** 	xvld	(\$xr[0-9]+),\$r5,0
+** 	xvfcmp.sule.d	(\$xr[0-9]+),\1,\2
+**	xvst	\3,\$r6,0
+**	jr	\$r1
+*/
+
+/*
+** compare_signaling_less_unordered:
+** 	xvld	(\$xr[0-9]+),\$r4,0
+** 	xvld	(\$xr[0-9]+),\$r5,0
+** 	xvfcmp.sult.d	(\$xr[0-9]+),\1,\2
+**	xvst	\3,\$r6,0
+**	jr	\$r1
+*/
+
+/*
+** compare_signaling_not_less:
+** 	xvld	(\$xr[0-9]+),\$r4,0
+** 	xvld	(\$xr[0-9]+),\$r5,0
+** 	xvfcmp.sule.d	(\$xr[0-9]+),\2,\1
+**	xvst	\3,\$r6,0
+**	jr	\$r1
+*/
+
+/*
+** compare_signaling_greater_unordered:
+** 	xvld	(\$xr[0-9]+),\$r4,0
+** 	xvld	(\$xr[0-9]+),\$r5,0
+** 	xvfcmp.sult.d	(\$xr[0-9]+),\2,\1
+**	xvst	\3,\$r6,0
+**	jr	\$r1
+*/
+
+/*
+** compare_quiet_less:
+** 	xvld	(\$xr[0-9]+),\$r4,0
+** 	xvld	(\$xr[0-9]+),\$r5,0
+** 	xvfcmp.clt.d	(\$xr[0-9]+),\1,\2
+**	xvst	\3,\$r6,0
+**	jr	\$r1
+*/
+
+/*
+** compare_quiet_less_equal:
+** 	xvld	(\$xr[0-9]+),\$r4,0
+** 	xvld	(\$xr[0-9]+),\$r5,0
+** 	xvfcmp.cle.d	(\$xr[0-9]+),\1,\2
+**	xvst	\3,\$r6,0
+**	jr	\$r1
+*/
+
+/*
+** compare_quiet_greater:
+** 	xvld	(\$xr[0-9]+),\$r4,0
+** 	xvld	(\$xr[0-9]+),\$r5,0
+** 	xvfcmp.clt.d	(\$xr[0-9]+),\2,\1
+**	xvst	\3,\$r6,0
+**	jr	\$r1
+*/
+
+/*
+** compare_quiet_greater_equal:
+** 	xvld	(\$xr[0-9]+),\$r4,0
+** 	xvld	(\$xr[0-9]+),\$r5,0
+** 	xvfcmp.cle.d	(\$xr[0-9]+),\2,\1
+**	xvst	\3,\$r6,0
+**	jr	\$r1
+*/
+
+/*
+** compare_quiet_not_less:
+** 	xvld	(\$xr[0-9]+),\$r4,0
+** 	xvld	(\$xr[0-9]+),\$r5,0
+** 	xvfcmp.cule.d	(\$xr[0-9]+),\2,\1
+**	xvst	\3,\$r6,0
+**	jr	\$r1
+*/
+
+/*
+** compare_quiet_greater_unordered:
+** 	xvld	(\$xr[0-9]+),\$r4,0
+** 	xvld	(\$xr[0-9]+),\$r5,0
+** 	xvfcmp.cult.d	(\$xr[0-9]+),\2,\1
+**	xvst	\3,\$r6,0
+**	jr	\$r1
+*/
+
+/*
+** compare_quiet_not_greater:
+** 	xvld	(\$xr[0-9]+),\$r4,0
+** 	xvld	(\$xr[0-9]+),\$r5,0
+** 	xvfcmp.cule.d	(\$xr[0-9]+),\1,\2
+**	xvst	\3,\$r6,0
+**	jr	\$r1
+*/
+
+/*
+** compare_quiet_less_unordered:
+** 	xvld	(\$xr[0-9]+),\$r4,0
+** 	xvld	(\$xr[0-9]+),\$r5,0
+** 	xvfcmp.cult.d	(\$xr[0-9]+),\1,\2
+**	xvst	\3,\$r6,0
+**	jr	\$r1
+*/
+
+/*
+** compare_quiet_unordered:
+** 	xvld	(\$xr[0-9]+),\$r4,0
+** 	xvld	(\$xr[0-9]+),\$r5,0
+** 	xvfcmp.cun.d	(\$xr[0-9]+),(\1,\2|\2,\1)
+**	xvst	\3,\$r6,0
+**	jr	\$r1
+*/
+
+/*
+** compare_quiet_ordered:
+** 	xvld	(\$xr[0-9]+),\$r4,0
+** 	xvld	(\$xr[0-9]+),\$r5,0
+** 	xvfcmp.cor.d	(\$xr[0-9]+),(\1,\2|\2,\1)
+**	xvst	\3,\$r6,0
+**	jr	\$r1
+*/
diff --git a/gcc/testsuite/gcc.target/loongarch/xvfcmp-f.c b/gcc/testsuite/gcc.target/loongarch/xvfcmp-f.c
index b54556475..1ca1e6c8b 100644
--- a/gcc/testsuite/gcc.target/loongarch/xvfcmp-f.c
+++ b/gcc/testsuite/gcc.target/loongarch/xvfcmp-f.c
@@ -1,27 +1,189 @@
 /* { dg-do compile } */
-/* { dg-options "-O2 -mlasx -ffixed-f0 -ffixed-f1 -ffixed-f2" } */
+/* { dg-options "-O2 -mlasx -fno-vect-cost-model" } */
+/* { dg-final { check-function-bodies "**" "" } } */
 
+#define F float
+#define I int
 #define VL 32
 
 #include "vfcmp-f.c"
 
-/* { dg-final { scan-assembler "compare_quiet_equal:.*\txvfcmp\\.ceq\\.s\t\\\$xr2,\\\$xr0,\\\$xr1.*-compare_quiet_equal\n" } } */
-/* { dg-final { scan-assembler "compare_quiet_not_equal:.*\txvfcmp\\.cune\\.s\t\\\$xr2,\\\$xr0,\\\$xr1.*-compare_quiet_not_equal\n" } } */
-/* { dg-final { scan-assembler "compare_signaling_greater:.*\txvfcmp\\.slt\\.s\t\\\$xr2,\\\$xr1,\\\$xr0.*-compare_signaling_greater\n" } } */
-/* { dg-final { scan-assembler "compare_signaling_greater_equal:.*\txvfcmp\\.sle\\.s\t\\\$xr2,\\\$xr1,\\\$xr0.*-compare_signaling_greater_equal\n" } } */
-/* { dg-final { scan-assembler "compare_signaling_less:.*\txvfcmp\\.slt\\.s\t\\\$xr2,\\\$xr0,\\\$xr1.*-compare_signaling_less\n" } } */
-/* { dg-final { scan-assembler "compare_signaling_less_equal:.*\txvfcmp\\.sle\\.s\t\\\$xr2,\\\$xr0,\\\$xr1.*-compare_signaling_less_equal\n" } } */
-/* { dg-final { scan-assembler "compare_signaling_not_greater:.*\txvfcmp\\.sule\\.s\t\\\$xr2,\\\$xr0,\\\$xr1.*-compare_signaling_not_greater\n" } } */
-/* { dg-final { scan-assembler "compare_signaling_less_unordered:.*\txvfcmp\\.sult\\.s\t\\\$xr2,\\\$xr0,\\\$xr1.*-compare_signaling_less_unordered\n" } } */
-/* { dg-final { scan-assembler "compare_signaling_not_less:.*\txvfcmp\\.sule\\.s\t\\\$xr2,\\\$xr1,\\\$xr0.*-compare_signaling_not_less\n" } } */
-/* { dg-final { scan-assembler "compare_signaling_greater_unordered:.*\txvfcmp\\.sult\\.s\t\\\$xr2,\\\$xr1,\\\$xr0.*-compare_signaling_greater_unordered\n" } } */
-/* { dg-final { scan-assembler "compare_quiet_less:.*\txvfcmp\\.clt\\.s\t\\\$xr2,\\\$xr0,\\\$xr1.*-compare_quiet_less\n" } } */
-/* { dg-final { scan-assembler "compare_quiet_less_equal:.*\txvfcmp\\.cle\\.s\t\\\$xr2,\\\$xr0,\\\$xr1.*-compare_quiet_less_equal\n" } } */
-/* { dg-final { scan-assembler "compare_quiet_greater:.*\txvfcmp\\.clt\\.s\t\\\$xr2,\\\$xr1,\\\$xr0.*-compare_quiet_greater\n" } } */
-/* { dg-final { scan-assembler "compare_quiet_greater_equal:.*\txvfcmp\\.cle\\.s\t\\\$xr2,\\\$xr1,\\\$xr0.*-compare_quiet_greater_equal\n" } } */
-/* { dg-final { scan-assembler "compare_quiet_not_less:.*\txvfcmp\\.cule\\.s\t\\\$xr2,\\\$xr1,\\\$xr0.*-compare_quiet_not_less\n" } } */
-/* { dg-final { scan-assembler "compare_quiet_greater_unordered:.*\txvfcmp\\.cult\\.s\t\\\$xr2,\\\$xr1,\\\$xr0.*-compare_quiet_greater_unordered\n" } } */
-/* { dg-final { scan-assembler "compare_quiet_not_greater:.*\txvfcmp\\.cule\\.s\t\\\$xr2,\\\$xr0,\\\$xr1.*-compare_quiet_not_greater\n" } } */
-/* { dg-final { scan-assembler "compare_quiet_less_unordered:.*\txvfcmp\\.cult\\.s\t\\\$xr2,\\\$xr0,\\\$xr1.*-compare_quiet_less_unordered\n" } } */
-/* { dg-final { scan-assembler "compare_quiet_unordered:.*\txvfcmp\\.cun\\.s\t\\\$xr2,\\\$xr0,\\\$xr1.*-compare_quiet_unordered\n" } } */
-/* { dg-final { scan-assembler "compare_quiet_ordered:.*\txvfcmp\\.cor\\.s\t\\\$xr2,\\\$xr0,\\\$xr1.*-compare_quiet_ordered\n" } } */
+/*
+** compare_quiet_equal:
+** 	xvld	(\$xr[0-9]+),\$r4,0
+** 	xvld	(\$xr[0-9]+),\$r5,0
+** 	xvfcmp.ceq.s	(\$xr[0-9]+),(\1,\2|\2,\1)
+**	xvst	\3,\$r6,0
+**	jr	\$r1
+*/
+
+/*
+** compare_quiet_not_equal:
+** 	xvld	(\$xr[0-9]+),\$r4,0
+** 	xvld	(\$xr[0-9]+),\$r5,0
+** 	xvfcmp.cune.s	(\$xr[0-9]+),(\1,\2|\2,\1)
+**	xvst	\3,\$r6,0
+**	jr	\$r1
+*/
+
+/*
+** compare_signaling_greater:
+** 	xvld	(\$xr[0-9]+),\$r4,0
+** 	xvld	(\$xr[0-9]+),\$r5,0
+** 	xvfcmp.slt.s	(\$xr[0-9]+),\2,\1
+**	xvst	\3,\$r6,0
+**	jr	\$r1
+*/
+
+/*
+** compare_signaling_greater_equal:
+** 	xvld	(\$xr[0-9]+),\$r4,0
+** 	xvld	(\$xr[0-9]+),\$r5,0
+** 	xvfcmp.sle.s	(\$xr[0-9]+),\2,\1
+**	xvst	\3,\$r6,0
+**	jr	\$r1
+*/
+
+/*
+** compare_signaling_less:
+** 	xvld	(\$xr[0-9]+),\$r4,0
+** 	xvld	(\$xr[0-9]+),\$r5,0
+** 	xvfcmp.slt.s	(\$xr[0-9]+),\1,\2
+**	xvst	\3,\$r6,0
+**	jr	\$r1
+*/
+
+/*
+** compare_signaling_less_equal:
+** 	xvld	(\$xr[0-9]+),\$r4,0
+** 	xvld	(\$xr[0-9]+),\$r5,0
+** 	xvfcmp.sle.s	(\$xr[0-9]+),\1,\2
+**	xvst	\3,\$r6,0
+**	jr	\$r1
+*/
+
+/*
+** compare_signaling_not_greater:
+** 	xvld	(\$xr[0-9]+),\$r4,0
+** 	xvld	(\$xr[0-9]+),\$r5,0
+** 	xvfcmp.sule.s	(\$xr[0-9]+),\1,\2
+**	xvst	\3,\$r6,0
+**	jr	\$r1
+*/
+
+/*
+** compare_signaling_less_unordered:
+** 	xvld	(\$xr[0-9]+),\$r4,0
+** 	xvld	(\$xr[0-9]+),\$r5,0
+** 	xvfcmp.sult.s	(\$xr[0-9]+),\1,\2
+**	xvst	\3,\$r6,0
+**	jr	\$r1
+*/
+
+/*
+** compare_signaling_not_less:
+** 	xvld	(\$xr[0-9]+),\$r4,0
+** 	xvld	(\$xr[0-9]+),\$r5,0
+** 	xvfcmp.sule.s	(\$xr[0-9]+),\2,\1
+**	xvst	\3,\$r6,0
+**	jr	\$r1
+*/
+
+/*
+** compare_signaling_greater_unordered:
+** 	xvld	(\$xr[0-9]+),\$r4,0
+** 	xvld	(\$xr[0-9]+),\$r5,0
+** 	xvfcmp.sult.s	(\$xr[0-9]+),\2,\1
+**	xvst	\3,\$r6,0
+**	jr	\$r1
+*/
+
+/*
+** compare_quiet_less:
+** 	xvld	(\$xr[0-9]+),\$r4,0
+** 	xvld	(\$xr[0-9]+),\$r5,0
+** 	xvfcmp.clt.s	(\$xr[0-9]+),\1,\2
+**	xvst	\3,\$r6,0
+**	jr	\$r1
+*/
+
+/*
+** compare_quiet_less_equal:
+** 	xvld	(\$xr[0-9]+),\$r4,0
+** 	xvld	(\$xr[0-9]+),\$r5,0
+** 	xvfcmp.cle.s	(\$xr[0-9]+),\1,\2
+**	xvst	\3,\$r6,0
+**	jr	\$r1
+*/
+
+/*
+** compare_quiet_greater:
+** 	xvld	(\$xr[0-9]+),\$r4,0
+** 	xvld	(\$xr[0-9]+),\$r5,0
+** 	xvfcmp.clt.s	(\$xr[0-9]+),\2,\1
+**	xvst	\3,\$r6,0
+**	jr	\$r1
+*/
+
+/*
+** compare_quiet_greater_equal:
+** 	xvld	(\$xr[0-9]+),\$r4,0
+** 	xvld	(\$xr[0-9]+),\$r5,0
+** 	xvfcmp.cle.s	(\$xr[0-9]+),\2,\1
+**	xvst	\3,\$r6,0
+**	jr	\$r1
+*/
+
+/*
+** compare_quiet_not_less:
+** 	xvld	(\$xr[0-9]+),\$r4,0
+** 	xvld	(\$xr[0-9]+),\$r5,0
+** 	xvfcmp.cule.s	(\$xr[0-9]+),\2,\1
+**	xvst	\3,\$r6,0
+**	jr	\$r1
+*/
+
+/*
+** compare_quiet_greater_unordered:
+** 	xvld	(\$xr[0-9]+),\$r4,0
+** 	xvld	(\$xr[0-9]+),\$r5,0
+** 	xvfcmp.cult.s	(\$xr[0-9]+),\2,\1
+**	xvst	\3,\$r6,0
+**	jr	\$r1
+*/
+
+/*
+** compare_quiet_not_greater:
+** 	xvld	(\$xr[0-9]+),\$r4,0
+** 	xvld	(\$xr[0-9]+),\$r5,0
+** 	xvfcmp.cule.s	(\$xr[0-9]+),\1,\2
+**	xvst	\3,\$r6,0
+**	jr	\$r1
+*/
+
+/*
+** compare_quiet_less_unordered:
+** 	xvld	(\$xr[0-9]+),\$r4,0
+** 	xvld	(\$xr[0-9]+),\$r5,0
+** 	xvfcmp.cult.s	(\$xr[0-9]+),\1,\2
+**	xvst	\3,\$r6,0
+**	jr	\$r1
+*/
+
+/*
+** compare_quiet_unordered:
+** 	xvld	(\$xr[0-9]+),\$r4,0
+** 	xvld	(\$xr[0-9]+),\$r5,0
+** 	xvfcmp.cun.s	(\$xr[0-9]+),(\1,\2|\2,\1)
+**	xvst	\3,\$r6,0
+**	jr	\$r1
+*/
+
+/*
+** compare_quiet_ordered:
+** 	xvld	(\$xr[0-9]+),\$r4,0
+** 	xvld	(\$xr[0-9]+),\$r5,0
+** 	xvfcmp.cor.s	(\$xr[0-9]+),(\1,\2|\2,\1)
+**	xvst	\3,\$r6,0
+**	jr	\$r1
+*/
-- 
2.43.0

